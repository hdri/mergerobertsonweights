//
//  main.cpp
//  tmp
//
//  Created by a on 28/06/2018.
//  Copyright © 2018 m-1. All rights reserved.
//

#include <iostream>
#include <cmath>

#define LDR_SIZE 256
//inline _LIBCPP_INLINE_VISIBILITY float       exp(float __lcpp_x) _NOEXCEPT       {return ::expf(__lcpp_x);}

void RobertsonWeights_v3_2_0(const std::function<void(int, float)> & doit)
{
	float q = (LDR_SIZE - 1) / 4.0f;

	for(int i = 0; i < LDR_SIZE; i++) {
		float value = i / q - 2.0f;
		value = exp(-value * value);
		//weight.at<Vec3f>(i) = Vec3f::all(value);
		//std::cout << i << '\t' << value << std::endl;
		doit(i, value);
	}
}
void RobertsonWeights_v3_4_0(const std::function<void(int, float)> & doit)
{
	float q = (LDR_SIZE - 1) / 4.0f;
	float e4 = exp(4.f);
	float scale = e4/(e4 - 1.f);
	float shift = 1 / (1.f - e4);
	
	for(int i = 0; i < LDR_SIZE; i++) {
		float value = i / q - 2.0f;
		value = scale*exp(-value * value) + shift;
		//weight.at<Vec3f>(i) = Vec3f::all(value);
		//std::cout << i << '\t' << value << std::endl;
		doit(i, value);
	}
}

float sum(const std::function<void(std::function<void(int, float)>)> & what) {
	float sum = 0;
	auto sumit = [&](int i, float value) {
		sum += value;
	};
	what(sumit);
	return sum;
}

int main(int argc, const char * argv[]) {
	{
		auto print = [](int i, float value) {
			std::cout << i << '\t' << value << std::endl;
		};
		RobertsonWeights_v3_2_0(print);
	}
	
	
	std::cout << sum(RobertsonWeights_v3_2_0) << std::endl;
	std::cout << sum(RobertsonWeights_v3_4_0) << std::endl;

	return 0;
}
